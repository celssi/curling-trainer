package com.curlingtrainer.backend.dao;

import com.curlingtrainer.backend.entities.Point;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface PointDAO extends CrudRepository<Point, Long> {
    Point findById(Long id);
}