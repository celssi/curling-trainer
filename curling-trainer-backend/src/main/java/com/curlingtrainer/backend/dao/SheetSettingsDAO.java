package com.curlingtrainer.backend.dao;

import com.curlingtrainer.backend.entities.SheetSettings;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface SheetSettingsDAO extends CrudRepository<SheetSettings, Long> {
    SheetSettings findById(Long id);
}