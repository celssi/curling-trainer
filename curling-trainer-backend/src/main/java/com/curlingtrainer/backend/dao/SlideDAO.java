package com.curlingtrainer.backend.dao;

import com.curlingtrainer.backend.entities.Slide;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface SlideDAO extends CrudRepository<Slide, Long> {
    Slide findById(Long id);
}