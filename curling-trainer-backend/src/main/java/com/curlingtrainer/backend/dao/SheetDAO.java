package com.curlingtrainer.backend.dao;

import com.curlingtrainer.backend.entities.Sheet;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface SheetDAO extends CrudRepository<Sheet, Long> {
    Sheet findById(Long id);
}