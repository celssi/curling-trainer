package com.curlingtrainer.backend.dao;

import com.curlingtrainer.backend.entities.Team;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface TeamDAO extends CrudRepository<Team, Long> {
    Team findById(Long id);
}