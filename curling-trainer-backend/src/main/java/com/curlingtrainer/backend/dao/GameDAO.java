package com.curlingtrainer.backend.dao;

import com.curlingtrainer.backend.entities.Game;
import com.curlingtrainer.backend.entities.Point;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface GameDAO extends CrudRepository<Game, Long> {
    Game findById(Long id);
}