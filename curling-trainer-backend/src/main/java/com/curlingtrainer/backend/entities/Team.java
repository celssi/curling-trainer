package com.curlingtrainer.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String teamName;

    @OneToMany(mappedBy = "team")
    @JsonIgnore
    private List<Slide> slides;

    @ManyToMany(mappedBy = "teams")
    @JsonIgnore
    private List<Game> games;

    public Team() {

    }

    public Team(String teamName) {
        this.teamName = teamName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    @JsonIgnore
    public List<Slide> getSlides() {
        return slides;
    }

    @JsonIgnore
    public void setSlides(List<Slide> slides) {
        this.slides = slides;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }
}
