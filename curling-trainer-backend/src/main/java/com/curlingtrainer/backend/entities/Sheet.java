package com.curlingtrainer.backend.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class Sheet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "sheet_settings_id")
    private SheetSettings sheetSettings;

    @OneToMany(mappedBy = "sheet")
    @JsonIgnore
    private List<Game> games;

    public Sheet() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SheetSettings getSheetSettings() {
        return sheetSettings;
    }

    public void setSheetSettings(SheetSettings sheetSettings) {
        this.sheetSettings = sheetSettings;
    }

    @JsonIgnore
    public List<Game> getGames() {
        return games;
    }

    @JsonIgnore
    public void setGames(List<Game> games) {
        this.games = games;
    }
}
