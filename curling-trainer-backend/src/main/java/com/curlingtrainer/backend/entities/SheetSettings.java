package com.curlingtrainer.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class SheetSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private boolean tournamentMode;

    private int actualSheetHeight;

    private int actualSheetWidth;

    private int aCircleWidth;

    private int bCircleWidth;

    private int cCircleWidth;

    private int dCircleWidth;

    private int actualFirstLinePosition;

    @OneToOne(mappedBy = "sheetSettings")
    @JsonIgnore
    private Sheet sheet;

    public SheetSettings() {
        tournamentMode = false;
        actualSheetHeight = 46973;
        actualSheetWidth = 4776;
        aCircleWidth = 606;
        bCircleWidth = 605;
        cCircleWidth = 437;
        dCircleWidth = 302;
        actualFirstLinePosition = 12000;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isTournamentMode() {
        return tournamentMode;
    }

    public void setTournamentMode(boolean tournamentMode) {
        this.tournamentMode = tournamentMode;
    }

    public Sheet getSheet() {
        return sheet;
    }

    public void setSheet(Sheet sheet) {
        this.sheet = sheet;
    }

    public int getActualSheetHeight() {
        return actualSheetHeight;
    }

    public void setActualSheetHeight(int actualSheetHeight) {
        this.actualSheetHeight = actualSheetHeight;
    }

    public int getActualSheetWidth() {
        return actualSheetWidth;
    }

    public void setActualSheetWidth(int actualSheetWidth) {
        this.actualSheetWidth = actualSheetWidth;
    }

    public int getaCircleWidth() {
        return aCircleWidth;
    }

    public void setaCircleWidth(int aCircleWidth) {
        this.aCircleWidth = aCircleWidth;
    }

    public int getbCircleWidth() {
        return bCircleWidth;
    }

    public void setbCircleWidth(int bCircleWidth) {
        this.bCircleWidth = bCircleWidth;
    }

    public int getcCircleWidth() {
        return cCircleWidth;
    }

    public void setcCircleWidth(int cCircleWidth) {
        this.cCircleWidth = cCircleWidth;
    }

    public int getdCircleWidth() {
        return dCircleWidth;
    }

    public void setdCircleWidth(int dCircleWidth) {
        this.dCircleWidth = dCircleWidth;
    }

    public int getActualFirstLinePosition() {
        return actualFirstLinePosition;
    }

    public void setActualFirstLinePosition(int actualFirstLinePosition) {
        this.actualFirstLinePosition = actualFirstLinePosition;
    }
}
