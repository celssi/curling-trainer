package com.curlingtrainer.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class Point {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private double x;

    @NotNull
    private double y;

    @NotNull
    private double direction;

    @ManyToOne
    @JoinColumn(name = "slide_id")
    @JsonIgnore
    private Slide slide;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date time;

    public Point() {

    }

    public Point(long x, long y, long direction, Slide slide, Date time) {
        this.slide = slide;
        this.x = x;
        this.y = y;
        this.direction = direction;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(long y) {
        this.y = y;
    }

    public double getDirection() {
        return direction;
    }

    public void setDirection(double direction) {
        this.direction = direction;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Slide getSlide() {
        return slide;
    }

    public void setSlide(Slide slide) {
        this.slide = slide;
    }
}
