package com.curlingtrainer.backend;

import com.curlingtrainer.backend.dao.*;
import com.curlingtrainer.backend.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class DataLoader implements ApplicationRunner {

    @Autowired
    private SheetDAO sheetDAO;

    @Autowired
    private GameDAO gameDAO;

    @Autowired
    private SlideDAO slideDAO;

    @Autowired
    private PointDAO pointDAO;

    @Autowired
    private TeamDAO teamDAO;

    private void loadData() {
        if (sheetDAO.findById(new Long(1l)) != null) {
            return;
        }

        SheetSettings sheetSettings = new SheetSettings();

        Sheet sheet = new Sheet();
        sheet.setSheetSettings(sheetSettings);
        sheet = sheetDAO.save(sheet);

        /*Team team = new Team("Team A");
        team = teamDAO.save(team);

        Team team2 = new Team("Team B");
        team2 = teamDAO.save(team2);

        Game game = new Game(sheet);

        ArrayList<Team> teams = new ArrayList<>();
        teams.add(team);
        teams.add(team2);
        game.setTeams(teams);

        game = gameDAO.save(game);

        Game game2 = new Game(sheet);
        game2.setTeams(teams);
        game2 = gameDAO.save(game2);

        Slide slide = new Slide();
        slide.setEnded(true);
        slide.setGame(game);
        slide.setStartedTime(new Date(new Date().getTime()));
        slide.setEndedTime(new Date(new Date().getTime() + 40000));
        slide.setTeam(team);
        slide.setDirection(false);

        slide = slideDAO.save(slide);

        Slide slide2 = new Slide();
        slide2.setEnded(false);
        slide2.setGame(game2);
        slide2.setStartedTime(new Date(new Date().getTime()));
        slide2.setTeam(team);
        slide2.setDirection(true);

        slide2 = slideDAO.save(slide2);

        Point p1 = new Point(4776 / 2 + 1500, 13000, 0, slide, new Date(new Date().getTime()));
        Point p2 = new Point(4776 / 2 + 1000, 20000, 0, slide, new Date(new Date().getTime() + 3000));
        Point p3 = new Point(4776 / 2 + 1000, 25000, 0, slide, new Date(new Date().getTime() + 6000));
        Point p4 = new Point(4776 / 2 + 700, 30000, 0,  slide, new Date(new Date().getTime() + 9000));
        Point p5 = new Point(4776 / 2 + 400, 35000, 0, slide, new Date(new Date().getTime() + 12000));
        Point p6 = new Point(4776 / 2 + 500, 42000, 0, slide, new Date(new Date().getTime() + 15000));

        List<Point> points = new ArrayList<>();
        points.add(p1);
        points.add(p2);
        points.add(p3);
        points.add(p4);
        points.add(p5);
        points.add(p6);

        CalculateDirections(points);

        p1 = pointDAO.save(p1);
        p2 = pointDAO.save(p2);
        p3 = pointDAO.save(p3);
        p4 = pointDAO.save(p4);
        p5 = pointDAO.save(p5);
        p6 = pointDAO.save(p6);

        Point p7 = new Point(4776 / 2 + 1400, 13000, 0, slide2, new Date(new Date().getTime()));
        Point p8 = new Point(4776 / 2 + 1600, 20000, 0, slide2, new Date(new Date().getTime() + 3000));
        Point p9 = new Point(4776 / 2 + 1100, 25000, 0, slide2, new Date(new Date().getTime() + 6000));
        Point p10 = new Point(4776 / 2 + 300, 35000, 0,  slide2, new Date(new Date().getTime() + 9000));
        Point p11 = new Point(4776 / 2 + 500, 42000, 0, slide2, new Date(new Date().getTime() + 12000));

        List<Point> points2 = new ArrayList<>();
        points2.add(p7);
        points2.add(p8);
        points2.add(p9);
        points2.add(p10);
        points2.add(p11);

        CalculateDirections(points2);

        p7 = pointDAO.save(p7);
        p8 = pointDAO.save(p8);
        p9 = pointDAO.save(p9);
        p10 = pointDAO.save(p10);
        p11 = pointDAO.save(p11);*/
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        loadData();
    }

    public static void CalculateDirections(List<Point> points) {
        Point previousPoint = null;

        for (Point point : points) {
            if (previousPoint == null) {
                previousPoint = point;
                continue;
            }

            previousPoint.setDirection(Math.atan((point.getY() - previousPoint.getY()) / (point.getX() - previousPoint.getX())));
            previousPoint.setDirection(previousPoint.getDirection() + ((previousPoint.getX() > point.getX()) ? -90 : 90) * Math.PI / 180);
            previousPoint = point;
        }

        if (points.size() > 1) {
            points.get(points.size()-1).setDirection(points.get(points.size()-2).getDirection());
        }
    }
}