package com.curlingtrainer.backend.controllers;

import com.curlingtrainer.backend.dao.*;
import com.curlingtrainer.backend.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.curlingtrainer.backend.DataLoader;

@RestController
public class DataController {
    @Autowired
    private GameDAO gameDAO;

    @Autowired
    private SheetDAO sheetDAO;

    @Autowired
    SlideDAO slideDAO;

    @Autowired
    PointDAO pointDAO;

    @Autowired
    TeamDAO teamDAO;

    @Autowired
    private SheetSettingsDAO sheetSettingsDAO;

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @MessageMapping("/live")
    @SendTo("/channel/public")
    public Game liveGame(@Payload Game game) {
        return game;
    }

    @RequestMapping(value = "/api/game/live", method = RequestMethod.POST)
    public ResponseEntity<Object> sendLiveGame(@RequestBody Game game) {
        Iterable<Slide> slides = slideDAO.save(game.getSlides());

        game.setSlides((List<Slide>) slides);
        game = gameDAO.save(game);

        for (Slide s : slides) {
            s.setGame(game);

            for (Point p : s.getPoints()) {
                p.setSlide(s);
            }

            DataLoader.CalculateDirections(s.getPoints());

            pointDAO.save(s.getPoints());
        }

        slideDAO.save(slides);

        messagingTemplate.convertAndSend("/channel/public", game);
        return new ResponseEntity<>(game, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/game/live/{sheetId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getEmptyGameForLive(@PathVariable Long sheetId) {
        Sheet sheet = sheetDAO.findById(sheetId);

        if (sheet != null) {
            Game result = new Game();
            result.setSheet(sheet);
            result.setStarted(new Date());

            return new ResponseEntity<>(result, HttpStatus.OK);
        }

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/api/game/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getGame(@PathVariable Long id) {
        Game result = gameDAO.findById(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/game/0/sheet/{sheetId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getGamesWithSheet(@PathVariable Long sheetId) {
        Sheet sheet = sheetDAO.findById(sheetId);
        List<Game> result = sheet.getGames();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/sheet", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getSheets() {
        List<Sheet> result = (ArrayList<Sheet>) sheetDAO.findAll();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/sheet-settings", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveSheetSettings(@RequestBody SheetSettings sheetSettings) {
        SheetSettings found = sheetSettingsDAO.findById(sheetSettings.getId());

        if (found != null) {
            sheetSettings = sheetSettingsDAO.save(sheetSettings);
            return new ResponseEntity<>(sheetSettings, HttpStatus.OK);
        }

        return new ResponseEntity<>(sheetSettings, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/api/team/{teamId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getTeams(@PathVariable Long teamId) {
        Team team = teamDAO.findById(teamId);
        return new ResponseEntity<>(team, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/team", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getTeams() {
        List<Team> result = (ArrayList<Team>) teamDAO.findAll();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
