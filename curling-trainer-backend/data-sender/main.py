# coding=utf-8
import json
import urllib2
import jsonpickle
import time
import random

class Point:
    def __init__(self, id, x, y, direction, time):
        self.id = id
        self.x = x
        self.y = y
        self.direction = direction
        self.time = time

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)


class Slide:
    def __init__(self, id, startedTime, ended, endedTime, direction):
        self.points = []
        self.id = id
        self.startedTime = startedTime
        self.ended = ended
        self.endedTime = endedTime
        self.direction = direction

    def addPoint(self, p):
        self.points.append(p)

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)


class Team:
    def __init__(self, id, teamName):
        self.id = id
        self.teamName = teamName

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)


class SheetSettings:
    def __init__(self, id, tournamentMode):
        self.id = id
        self.tournamentMode = tournamentMode

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)


class Sheet:
    def __init__(self, id, sheetSettings):
        self.id = id
        self.sheetSettings = sheetSettings

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)


class Game:
    def __init__(self, id, started, sheet):
        self.id = id
        self.started = started
        self.sheet = sheet
        self.teams = []
        self.slides = []

    def addTeam(self, team):
        self.teams.append(team)

    def addSlide(self, slide):
        self.slides.append(slide)

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)


def json_to_game(json):
    json = jsonpickle.decode(json)

    sheetSettings = SheetSettings(json['sheet']['sheetSettings']['id'], json['sheet']['sheetSettings']['tournamentMode'])
    sheet = Sheet(json['sheet']['id'], sheetSettings)

    game = Game(json['id'], json['started'], sheet)

    slidesJson = json['slides']

    if slidesJson is not None:
        for slideJson in slidesJson:
            slide = Slide(slideJson['id'], slideJson['startedTime'], slideJson['ended'], slideJson['endedTime'], slideJson['direction'])

            pointsJson = slideJson['points']

            if pointsJson is not None:
                for pointJson in pointsJson:
                    point = Point(pointJson['id'], pointJson['x'], pointJson['y'], pointJson['direction'], pointJson['time'])
                    slide.addPoint(point)

            game.addSlide(slide)

    print jsonpickle.encode(game, unpicklable=False)
    print '\r\n'
    return game

# Tällä valitaan, mikä kenttä on käytössä
selected_sheet_id = 1

# Haetaan pelin json-runko
request = urllib2.Request('http://localhost:8989/api/game/live/' + str(selected_sheet_id))
game = urllib2.urlopen(request).read()
game = json_to_game(game)
game.id = -1

for i in range(0, 10):
    slide = Slide(0, 1511348572000, False, None, True if i % 2 == 0 else False)
    game.addSlide(slide)

    # Tässä luodaan kasa pisteitä testaamista varten
    
    points = []
    for x in range(0, 30):
        points.append(Point(0, random.randint(4776 / 2 + 800, 4776 / 2 + 1300), 13000 + x*1000, None, 1511348572000 + x*500))
        
    for j in range(0, 30):
        game.slides[len(game.slides)-1].addPoint(points[j])

        if j == 5:
            game.slides[len(game.slides) - 1].ended = True
            game.slides[len(game.slides) - 1].endedTime = 1511355138000

        data = jsonpickle.encode(game, unpicklable=False)

        # print data

        req = urllib2.Request('http://localhost:8989/api/game/live')
        req.add_header('Content-Type', 'application/json')
        response = urllib2.urlopen(req, data)

        game = json_to_game(response.read())

        time.sleep(0.5)

    time.sleep(3)
