import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurlingSheetComponent } from './curling-sheet.component';

describe('CurlingSheetComponent', () => {
  let component: CurlingSheetComponent;
  let fixture: ComponentFixture<CurlingSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurlingSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurlingSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
