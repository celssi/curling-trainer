import {
  Component, ViewChild, Input, SimpleChanges, OnChanges, OnDestroy, OnInit, HostListener,
  AfterViewInit
} from '@angular/core';
import {Point} from '../point';
import {Slide} from '../slide';
import {Game} from '../game';
import {Observable, Subscription} from 'rxjs'
import {Settings} from '../settings';
import {SettingsService} from '../settings-service.service';
import {SheetSettings} from '../sheet-settings';

@Component({
  selector: 'curling-sheet',
  templateUrl: './curling-sheet.component.html',
  styleUrls: ['./curling-sheet.component.css']
})
export class CurlingSheetComponent implements OnChanges, OnDestroy {
  @Input('selectedSlide')
  public selectedSlide: Slide;

  @Input('selectedGame')
  public selectedGame: Game;

  public sheetSettings: SheetSettings;

  height: number;
  width: number;
  speed: number = 0;
  totalSpeed: number = 0;
  angle: number = 0;
  private firstLinePosition;

  private animationStartTime: number;
  private animationSubscription: Subscription;
  private timerValue: number = 0;
  private timerSubscription: Subscription;

  private settingsSubscription: Subscription;
  settings: Settings = new Settings();
  smallScreen: boolean = false;

  context: CanvasRenderingContext2D;
  @ViewChild('curlingCanvas') canvas;

  context2: CanvasRenderingContext2D;
  @ViewChild('curlingCanvas2') canvas2;

  constructor(private settingsService: SettingsService) {
    this.settingsService = settingsService;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.selectedGame) {
      this.context = this.canvas.nativeElement.getContext('2d');
      this.context2 = this.canvas2.nativeElement.getContext('2d');

      this.sheetSettings = this.selectedGame.sheet.sheetSettings;

      this.smallScreen = window.innerWidth < 800;
      this.height = window.innerHeight - (this.smallScreen ? -200 : 200);
      this.width = this.height * (this.sheetSettings.actualSheetWidth / this.sheetSettings.actualSheetHeight);
      this.firstLinePosition = this.height * (this.sheetSettings.actualFirstLinePosition / this.sheetSettings.actualSheetHeight);

      setTimeout(() => this.draw(), 0);

      this.settingsSubscription = this.settingsService.getSettings().subscribe(settingsContainer => {
        this.settings = settingsContainer.settings;
        this.draw();
      });
    }
  }

  ngOnDestroy(): void {
    this.settingsSubscription.unsubscribe();
    this.animationSubscription.unsubscribe();
    this.timerSubscription.unsubscribe();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.smallScreen = window.innerWidth < 800;
  }

  private draw = function() {
    this.speed = 0;
    this.totalSpeed = 0;
    this.angle = 0;

    if (this.settings.runAsAnimation) {
      this.startDrawAsAnimation(this.context);
    } else {
      if (this.context) {
        this.drawSheet(this.context);
        this.drawSlide(this.context);
      }

      if (this.context2) {
        this.drawSheet(this.context2);
        this.drawPrediction(this.context2);
      }
    }
  };

  private drawSheet = function (context) {
    context.clearRect(0, 0, this.width, this.height);
    this.data = [];

    // Ylempi pesä
    this.drawCircle(this.width / 2, this.width / 2, this.width * ((this.sheetSettings.aCircleWidth + this.sheetSettings.bCircleWidth + this.sheetSettings.cCircleWidth + this.sheetSettings.dCircleWidth) / this.sheetSettings.actualSheetWidth), '#0000FF', context);
    this.drawCircle(this.width / 2, this.width / 2, this.width * ((this.sheetSettings.bCircleWidth + this.sheetSettings.cCircleWidth + this.sheetSettings.dCircleWidth) / this.sheetSettings.actualSheetWidth), '#FFFFFF', context);
    this.drawCircle(this.width / 2, this.width / 2, this.width * ((this.sheetSettings.cCircleWidth + this.sheetSettings.dCircleWidth) / this.sheetSettings.actualSheetWidth), '#FF0000', context);
    this.drawCircle(this.width / 2, this.width / 2, this.width * ((this.sheetSettings.dCircleWidth) / this.sheetSettings.actualSheetWidth), '#FFFFFF', context);

    // Alempi pesä
    this.drawCircle(this.width / 2, this.height - this.width / 2, this.width * ((this.sheetSettings.aCircleWidth + this.sheetSettings.bCircleWidth + this.sheetSettings.cCircleWidth + this.sheetSettings.dCircleWidth) / this.sheetSettings.actualSheetWidth), '#0000FF', context);
    this.drawCircle(this.width / 2, this.height - this.width / 2, this.width * ((this.sheetSettings.bCircleWidth + this.sheetSettings.cCircleWidth + this.sheetSettings.dCircleWidth) / this.sheetSettings.actualSheetWidth), '#FFFFFF', context);
    this.drawCircle(this.width / 2, this.height - this.width / 2, this.width * ((this.sheetSettings.cCircleWidth + this.sheetSettings.dCircleWidth) / this.sheetSettings.actualSheetWidth), '#FF0000', context);
    this.drawCircle(this.width / 2, this.height - this.width / 2, this.width * ((this.sheetSettings.dCircleWidth) / this.sheetSettings.actualSheetWidth), '#FFFFFF', context);

    // Paksut viivat
    this.drawLine(0, this.firstLinePosition, this.width, this.firstLinePosition, '#000000', 2, context);
    this.drawLine(0, this.height - this.firstLinePosition, this.width, this.height - this.firstLinePosition, '#000000', 2, context);

    // Apuviivat
    this.drawLine(this.width / 2, 0, this.width / 2, this.height, '#666666', 1, context);
    this.drawLine(0, this.height / 2, this.width, this.height / 2, '#666666', 1, context);
    this.drawLine(0, this.width / 2, this.width, this.width / 2, '#666666', 1, context);
    this.drawLine(0, this.height - this.width / 2, this.width, this.height - this.width / 2, '#666666', 1, context);
  };

  private drawSlide = function (context) {
    if (this.selectedSlide != undefined && this.selectedSlide.points.length > 0) {
      let points: Point[] = this.selectedSlide.points;

      let i = 1;
      for (let point of points) {
        // https://stackoverflow.com/questions/11313214/convert-point-from-coordinate-system-0-800-to-coordinate-system-50-50-jfram
        let px: number = (point.x / this.sheetSettings.actualSheetWidth) * this.width;
        let py: number = (point.y / this.sheetSettings.actualSheetHeight) * this.height;
        this.addPoint(px, py, 7, context);

        let drawPrediction: boolean = i > 1 && i == points.length && !this.selectedSlide.ended;

        if (point.direction != undefined) {
          this.drawArrow(px, py, point.direction, drawPrediction, context);
        }

        i++;
      }
    }
  };

  private drawPrediction(context) {
    if (this.selectedSlide != undefined && this.selectedSlide.points.length > 0) {
      let points: Point[] = this.selectedSlide.points;

      if (points.length > 2) {
        let pointA = points[0];
        let pointB = points[2];
        let lastPoint = points[points.length-1];

        let a = pointA.x - pointB.x;
        let b = pointA.y - pointB.y;
        let c = pointA.x - lastPoint.x;
        let d = pointA.y - lastPoint.y;
        let speed = Math.sqrt(a * a + b * b) / (Number(pointB.time) - Number(pointA.time));
        let speed2 = Math.sqrt(c * c + d * d) / (Number(lastPoint.time) - Number(pointA.time));
        this.speed = speed;
        this.totalSpeed = speed2;

        let px: number = (pointA.x / this.sheetSettings.actualSheetWidth) * this.width;
        let py: number = (pointA.y / this.sheetSettings.actualSheetHeight) * this.height;

        let tempAngle = Math.atan((pointB.y - pointA.y) / (pointB.x - pointA.x));
        tempAngle = tempAngle + ((pointA.x > pointB.x) ? -90 : 90) * Math.PI / 180;
        this.angle = tempAngle * (180/Math.PI) + 180;

        if (this.angle >= 270) {
          this.angle = this.angle - 360;
        }

        this.drawDot({x: px, y: py, size: 7}, context, true, false);
        this.drawLongArrow(px, py, tempAngle, context);
        //this.drawSpeed(px, py, speed.toFixed(1), this.selectedSlide.direction, context);
      }
    }
  }

  private addPoint = function (x, y, size, context) {
    this.data.push({x: x, y: y, size: size});
    this.drawPoint(this.data, context);
  };

  private drawPoint = function (data, context) {
    for (let i = 0; i < data.length; i++) {
      this.drawDot(data[i], context, i == 0, i == data.length - 1);

      if (i > 0) {
        this.drawLineBetweenPoints(data[i], data[i - 1], context);
      }
    }
  };

  private drawDot = function (point, context, isFirst: boolean, isLast: boolean) {
    context.beginPath();
    context.arc(point.x, point.y, point.size, 0, 2 * Math.PI, false);
    context.fillStyle = isFirst ? '#006400' : (isLast ? '#b22222' : '#ccddff');
    context.fill();
    context.lineWidth = 1;
    context.strokeStyle = '#666666';
    context.stroke();
  };

  private drawLineBetweenPoints = function (pointA, pointB, context) {
    context.beginPath();
    context.moveTo(pointA.x, pointA.y);
    context.lineTo(pointB.x, pointB.y);
    context.strokeStyle = '#000000';
    context.stroke();
  };

  private drawCircle = function (x, y, size, color, context) {
    context.beginPath();
    context.arc(x, y, size, 0, 2 * Math.PI, false);
    context.fillStyle = color;
    context.fill();
    context.lineWidth = 1;
    context.strokeStyle = '#000000';
    context.stroke();
  };

  private drawLine = function (fromX, fromY, toX, toY, color, width, context) {
    context.beginPath();
    context.moveTo(fromX, fromY);
    context.lineTo(toX, toY);
    context.strokeStyle = color;
    context.lineWidth = width;
    context.stroke();
  };

  private drawArrow = function (x, y, angle, isLast, context) {
    if (isLast) {
      let x2 = x + Math.cos(Math.PI / 2 + angle) * -120;
      let y2 = y + Math.sin(Math.PI / 2 + angle) * -120;

      context.lineWidth = 2;
      context.strokeStyle = '#00FF00';
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(x2, y2);
      context.stroke();
    }
  };

  private drawLongArrow = function (x, y, angle, context) {
    let x2 = x + Math.cos(Math.PI / 2 + angle) * -(this.height - this.firstLinePosition);
    let y2 = y + Math.sin(Math.PI / 2 + angle) * -(this.height - this.firstLinePosition);

    context.lineWidth = 2;
    context.strokeStyle = '#4cff00';
    context.beginPath();
    context.moveTo(x, y);
    context.lineTo(x2, y2);
    context.stroke();
  };

  private drawSpeed = function(x, y, speed, direction, context) {
    context.fillStyle = '#000000';
    context.font = '13px Arial';

    let rightSide: boolean = (x + 10 + 35) < this.width;

    context.save();
    context.translate(rightSide ? x + 10 : x - 35, y);

    if (direction) {
      context.rotate(Math.PI);
    }

    context.textAlign = 'center';
    context.fillText(speed + 'm/s', 0, 5);

    context.restore();
  };

  private drawNextFrame = function (context) {
    this.drawSheet(context);

    if (this.selectedSlide.points.length > 0) {
      let currentTime: number = (new Date()).getTime();
      let timeDiff: number = currentTime - this.animationStartTime;
      let slideStartTime: number = this.selectedSlide.points[0].time;
      let selectedPointA: Point;
      let selectedPointB: Point;
      let previousPoint: Point;
      let lastPoint: Point = this.selectedSlide.points[this.selectedSlide.points.length - 1];

      if (timeDiff > <any>lastPoint.time - slideStartTime) {
        this.addPoint(lastPoint.x, lastPoint.y, 7, context);
        this.settings.runAsAnimation = false;
        this.settingsService.sendSettings(this.settings);
        return;
      }

      let i = 0;
      for (let point of this.selectedSlide.points) {
        if (previousPoint == undefined) {
          previousPoint = point;
          continue;
        }

        let currentPointTimeDiff: number = point.time - slideStartTime;
        let previousPointTimeDiff: number = <any>previousPoint.time - slideStartTime;

        if (timeDiff > previousPointTimeDiff && timeDiff < currentPointTimeDiff) {
          selectedPointA = previousPoint;
          selectedPointB = point;
        }

        previousPoint = point;
      }

      if (selectedPointA != undefined && selectedPointB != undefined) {
        let selectedPointATimeDiff: number = <any>selectedPointA.time - slideStartTime;
        let selectedPointBTimeDiff: number = <any>selectedPointB.time - slideStartTime;
        let percentage: number = (timeDiff - selectedPointATimeDiff) / (selectedPointBTimeDiff - selectedPointATimeDiff);
        let coordinates = [Math.round(selectedPointA.y + (selectedPointB.y - selectedPointA.y) * percentage), Math.round(selectedPointA.x + (selectedPointB.x - selectedPointA.x) * percentage)];
        let px: number = (coordinates[1] / this.sheetSettings.actualSheetWidth) * this.width;
        let py: number = (coordinates[0] / this.sheetSettings.actualSheetHeight) * this.height;

        this.addPoint(px, py, 7, context);
      }


      if (i === 1) {
        this.drawPrediction(this.context2);
      }

      ++i;
    }
  };

  private startDrawAsAnimation = function (context) {
    this.animationStartTime = (new Date()).getTime();
    this.timerValue = 0;

    this.timerSubscription = Observable.interval(1000)
      .takeWhile(() => this.settings.runAsAnimation)
      .subscribe(t => {
        this.timerValue = (t + 1) * 1000;
      });

    this.animationSubscription = Observable.interval(100)
      .takeWhile(() => this.settings.runAsAnimation)
      .subscribe(t => {
        this.drawNextFrame(context);
      });
  };

}
