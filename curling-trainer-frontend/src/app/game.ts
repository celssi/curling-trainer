import {Sheet} from './sheet';
import {Team} from './team';
import {Slide} from './slide';

export class Game {
  id: number;
  started: Date;
  ended: Date;
  sheet: Sheet;
  teams: Team[];
  slides: Slide[];

  constructor(id: number, started: Date, sheet: Sheet) {
   this.id = id;
   this.started = started;
   this.sheet = sheet;
   this.teams = [];
   this.slides = [];
  }

  public addTeam(team: Team) {
    this.teams.push(team);
  }

  public addSlide(slide: Slide) {
    this.slides.push(slide);
  }
}
