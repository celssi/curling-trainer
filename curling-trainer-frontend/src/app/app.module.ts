import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderNavComponent } from './header-nav/header-nav.component';
import { CurlingSheetComponent } from './curling-sheet/curling-sheet.component';
import { SelectToolsComponent } from './select-tools/select-tools.component';
import {DataService} from "./data-service.service";
import {SettingsService} from "./settings-service.service";
import {HttpClientModule} from "@angular/common/http";

import { StompService } from 'ng2-stomp-service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderNavComponent,
    CurlingSheetComponent,
    SelectToolsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    DataService,
    SettingsService,
    StompService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
