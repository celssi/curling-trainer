import {SheetSettings} from "./sheet-settings";

export class Sheet {
  id: number;
  sheetSettings: SheetSettings;

  constructor(id: number) {
    this.id = id;
  }

  setSheetSettings(sheetSetting: SheetSettings) {
    this.sheetSettings = sheetSetting;
  }
}
