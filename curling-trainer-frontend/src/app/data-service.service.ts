import {Injectable} from '@angular/core';
import {Game} from './game';
import {Sheet} from './sheet';
import {SheetSettings} from './sheet-settings';
import {HttpClient} from '@angular/common/http';
import {StompService} from 'ng2-stomp-service';

@Injectable()
export class DataService {
  public stomp: StompService;
  private serverIp = 'http://localhost:8989';

  constructor(private http: HttpClient, private stompService: StompService) {
    this.stomp = stompService;

    this.stomp.configure({
      host: this.serverIp + '/ws',
      debug: true,
      queue: {'init': false}
    });
  }

  getGames(sheetId: number): Promise<Game[]> {
    return this.http.get(this.serverIp + '/api/game/0/sheet/' + sheetId).toPromise();
  }

  getSheets(): Promise<Sheet[]> {
    return this.http.get(this.serverIp + '/api/sheet').toPromise();
  }

  saveSheetSettings(sheetSettings: SheetSettings): Promise<SheetSettings> {
    return this.http.post(this.serverIp + '/api/sheet-settings', sheetSettings).toPromise();
  }

  sendLive(game: Game) {
    this.stomp.send('/app/live', game);
  }
}
