import {Point} from "./point";
import {Team} from "./team";

export class Slide {
  id: number;
  team: Team;
  ended: boolean;
  direction: boolean = false;
  points: Point[];

  constructor(id: number, team: Team) {
    this.id = id;
    this.team = team;
    this.points = [];
    this.ended = false;
  }

  addPoint(point: Point) {
    this.points.push(point);

    let previousPoint: Point;
    for (point of this.points) {
      if (previousPoint == undefined) {
        previousPoint = point;
        continue;
      }

      previousPoint.direction = Math.atan((point.y - previousPoint.y) / (point.x - previousPoint.x));
      previousPoint.direction += ((previousPoint.x > point.x) ? -90 : 90) * Math.PI / 180;
      previousPoint = point;
    }

    if (this.points.length > 1) {
      this.points[this.points.length-1].direction = this.points[this.points.length-2].direction;
    }
  }
}
