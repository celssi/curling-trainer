import {Component, ElementRef, EventEmitter, Input, OnDestroy, Output, ViewChild} from '@angular/core';
import {Game} from '../game';
import {Slide} from '../slide';
import {SettingsService} from '../settings-service.service';
import {Settings} from '../settings';
import {Subscription} from 'rxjs/Subscription';
import {Sheet} from '../sheet';
import {DataService} from '../data-service.service';

@Component({
  selector: 'select-tools',
  templateUrl: './select-tools.component.html',
  styleUrls: ['./select-tools.component.css']
})
export class SelectToolsComponent implements OnDestroy {
  selectedGameValue: Game;
  selectedSlideValue: Slide;
  selectedSheetValue: Sheet;
  selectedDayValue: Date;
  settings: Settings = new Settings();
  settingsOpen: boolean = false;

  private liveSubscription: Subscription;
  private settingsSubscription: Subscription;

  @ViewChild('settingsButton')
  settingsButton: ElementRef;

  sheets: Sheet[];
  games: Game[];
  trainingGames: Game[];
  days: Date[];

  @Input()
  get selectedSlide() {
    return this.selectedSlideValue;
  }

  @Input()
  get selectedGame() {
    return this.selectedGameValue;
  }

  @Input()
  get selectedSheet() {
    return this.selectedSheetValue;
  }

  @Input()
  get selectedDay() {
    return this.selectedDayValue;
  }

  @Output() selectedSlideChange = new EventEmitter();
  @Output() selectedGameChange = new EventEmitter();
  @Output() selectedSheetChange = new EventEmitter();
  @Output() selectedDayChange = new EventEmitter();

  set selectedSlide(val) {
    this.selectedSlideValue = val;
    this.selectedSlideChange.emit(this.selectedSlideValue);
  }

  set selectedGame(val) {
    this.selectedGameValue = val;
    this.selectedGameChange.emit(this.selectedGameValue);
  }

  set selectedSheet(val) {
    this.selectedSheetValue = val;
    this.selectedSheetChange.emit(this.selectedSheetValue);
  }

  set selectedDay(val) {
    this.selectedDayValue = val;
    this.selectedDayChange.emit(this.selectedDayValue);
  }

  constructor(private settingsService: SettingsService, private dataService: DataService) {
    this.settingsSubscription = this.settingsService.getSettings().subscribe(settingsContainer => {
      this.settings = settingsContainer.settings;
    });

    this.getGameDataIfNeeded();
  }

  ngOnDestroy(): void {
    this.settingsSubscription.unsubscribe();
    this.liveSubscription.unsubscribe();
  }

  private responseFromLive = (data: Game) => {
    if (!this.trainingGames.some(g => g.id == data.id)) {
      this.trainingGames.push(data);
    } else {
      let dataInArray: Game = this.trainingGames.filter(x => x.id == data.id)[0];
      let index = this.trainingGames.indexOf(dataInArray);
      this.trainingGames[index] = data;
    }

    let gameDate = new Date(data.started);
    gameDate.setHours(0, 0, 0, 0);

    if (!this.days.some(d => d.getTime() == gameDate.getTime())) {
      this.days.push(gameDate);
    } else {
      let dataInArray = this.days.filter(x => x.getTime() == gameDate.getTime())[0];
      let index = this.days.indexOf(dataInArray);
      this.days[index] = gameDate;
    }

    this.selectedDay = gameDate;
    this.selectedGame = data;
    this.setLastSlide();
  };

  private setDefaultGame = function () {
    if (this.selectedSheet !== undefined) {
      this.dataService.getGames(this.selectedSheet.id).then((games: Game[]) => {
        this.games = games.filter(v => v.teams && v.teams.length === 2);
        this.trainingGames = games.filter(v => !v.teams || v.teams.length < 2);

        if (this.games && this.games.length > 0 && this.selectedSheet.sheetSettings.tournamentMode) {
          this.selectedGame = this.games[this.games.length - 1];
          this.setDefaultSlide();
        } else if (this.trainingGames && this.trainingGames.length > 0 && !this.selectedSheet.sheetSettings.tournamentMode) {
          this.selectedGame = this.trainingGames[this.trainingGames.length - 1];
          this.setDefaultSlide();
        } else {
          this.selectedGame = undefined;
          this.selectedSlide = undefined;
        }
      });
    }
  };

  private setDefaultSlide = function () {
    if (this.selectedGame && this.selectedGame.slides.length > 0) {
      this.selectedSlide = this.selectedGame.slides[this.selectedGame.slides.length - 1];
      this.setDirection();
    }
  };

  private setLastSlide = function () {
    if (this.selectedGame.slides.length > 0) {
      this.selectedSlide = this.selectedGame.slides[this.selectedGame.slides.length - 1];
      this.setDirection();
    }
  };

  sheetChanged = function (id: number) {
    if (this.sheets.length > 0) {
      this.setDefaultGame();
    }
  };

  gameChanged = function (data) {
    this.setDefaultSlide();
  };

  slideChanged = function (data) {
    this.setDirection();
  };

  dayChanged = function (day: Date) {
    this.dataService.getGames(this.selectedSheet.id).then((games: Game[]) => {
      this.trainingGames = games.filter((g: Game) => {
        let gameDate = new Date(g.started);
        gameDate.setHours(0, 0, 0, 0);

        let dateSelected = new Date(day);
        dateSelected.setHours(0, 0, 0, 0);

        return gameDate.getTime() == dateSelected.getTime() && (!g.teams || g.teams.length < 2);
      });

      if (this.trainingGames && this.trainingGames.length > 0 && !this.selectedSheet.sheetSettings.tournamentMode) {
        this.setDefaultGame();
      } else {
        this.selectedGame = undefined;
        this.selectedSlide = undefined;
      }
    });
  };

  toggleReplay = function () {
    this.settings.runAsAnimation = !this.settings.runAsAnimation;
    this.settingsService.sendSettings(this.settings);

    if (this.settings.runAsAnimation) {
      if (this.settingsButton.nativeElement.offsetParent != null) {
        this.settingsButton.nativeElement.click();
      }

      this.settingsOpen = false;
      window.scrollTo(0, 0);
    }
  };

  flippedChanged = function (value: number) {
    this.settings.flipped = value == 1;
    this.settingsService.sendSettings(this.settings);
  };

  toggleSettings = function () {
    this.settingsOpen = !this.settingsOpen;
  };

  gameModeChanged = function () {
    this.selectedSheet.sheetSettings.tournamentMode = !this.selectedSheet.sheetSettings.tournamentMode;

    this.dataService.saveSheetSettings(this.selectedSheet.sheetSettings).then(() => {
      this.selectedGame = undefined;
      this.selectedSlide = undefined;
      this.getGameDataIfNeeded();
    });
  };

  getGameDataIfNeeded = function () {

    this.dataService.getSheets().then(sheets => {
      this.sheets = sheets;

      if (this.sheets && this.sheets.length > 0) {
        this.selectedSheet = this.sheets[0];
        this.toggleLiveSubscription(false);
      }

      return this.dataService.getGames(this.selectedSheet.id);
    }).then(games => {

      if (this.selectedSheet && this.selectedSheet.sheetSettings && this.selectedSheet.sheetSettings.tournamentMode) {
        this.games = games.filter(v => v.teams && v.teams.length === 2);
        this.trainingGames = undefined;

        if (this.games && this.games.length > 0) {
          this.selectedGame = this.games[this.games.length - 1];
          this.setDefaultSlide();
        }
      } else if (this.selectedSheet) {
        this.trainingGames = games.filter(v => !v.teams || v.teams.length < 2);
        this.games = undefined;

        this.days = [];
        this.trainingGames.forEach((game: Game) => {
          let gameDate = new Date(game.started);
          gameDate.setHours(0, 0, 0, 0);

          if (!this.days.some(d => d.getTime() === gameDate.getTime())) {
            this.days.push(gameDate);
          }
        });

        this.days.sort();

        this.selectedDay = this.days[this.days.length - 1];

        this.trainingGames = games.filter((g: Game) => {
          let gameDate = new Date(g.started);
          gameDate.setHours(0, 0, 0, 0);

          let dateSelected = new Date(this.selectedDay);
          dateSelected.setHours(0, 0, 0, 0);

          return gameDate.getTime() == dateSelected.getTime() && (!g.teams || g.teams.length < 2);
        });

        this.selectedGame = this.trainingGames[this.trainingGames.length - 1];
        this.setDefaultSlide();
      }
    });
  };

  toggleLiveSubscription = function (reloadGameData: boolean) {
    if (this.selectedSheet && this.selectedSheet.sheetSettings && !this.selectedSheet.sheetSettings.tournamentMode) {
      this.dataService.stomp.startConnect().then(() => {
        this.dataService.stomp.done('init');
        this.liveSubscription = this.dataService.stomp.subscribe('/channel/public', this.responseFromLive);
      });
    } else if (this.liveSubscription) {
      this.liveSubscription.unsubscribe();

      if (reloadGameData) {
        this.getGameDataIfNeeded();
      }
    }
  };

  setDirection = function () {
    this.settings.flipped = this.selectedSlide.direction;
    this.settingsService.sendSettings(this.settings);
  };
}
