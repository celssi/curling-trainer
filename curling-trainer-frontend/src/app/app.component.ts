import { Component } from '@angular/core';
import {Slide} from "./slide";
import {Game} from "./game";
import {DataService} from "./data-service.service";
import {Sheet} from "./sheet";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.css'
  ]
})
export class AppComponent {
  public selectedSheet: Sheet;
  public selectedGame: Game;
  public selectedSlide: Slide;

  changeSelectedSheet = function (selectedSheet) {
    this.selectedSheet = selectedSheet;
  };

  changeSelectedGame = function (selectedGame) {
    this.selectedGame = selectedGame;
  };

  changeSelectedSlide = function (selectedSlide) {
    this.selectedSlide = selectedSlide;
  };
}
