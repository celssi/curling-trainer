import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import {Settings} from "./settings";

@Injectable()
export class SettingsService {
  private subject = new Subject<any>();

  sendSettings(settings: Settings) {
    this.subject.next({ settings: settings });
  }

  clearSettings() {
    this.subject.next();
  }

  getSettings(): Observable<any> {
    return this.subject.asObservable();
  }
}
