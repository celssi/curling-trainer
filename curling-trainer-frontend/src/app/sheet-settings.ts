export class SheetSettings {
  id: number;
  sheet_id: number;
  tournamentMode: boolean = false;
  actualSheetHeight: number;
  actualSheetWidth: number;
  aCircleWidth: number;
  bCircleWidth: number;
  cCircleWidth: number;
  dCircleWidth: number;
  actualFirstLinePosition: number;


  constructor(id: number, sheet_id: number) {
    this.id = id;
    this.sheet_id = sheet_id;
  }
}
