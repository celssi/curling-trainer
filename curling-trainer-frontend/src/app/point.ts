export class Point {
  id: number;
  x: number;
  y: number;
  direction: number;
  time: Date;

  constructor(id: number, x: number, y: number, time: Date) {
    this.id = id;
    this.x = x;
    this.y = y;
    this.time = time;
  }
}
