## Curling Trainer
Tämä on erittäin hieno projekti, jonka tarkoituksena on kehittää curlingin harjoittelemiseen tarkoitettu työkalu.

## Git
Kun kehität jotain uutta, luo gitissä haara, jonka nimenä tehtävän koodi.
Kun saat tehtävän valmiiksi, tee siitä merge request ja laita tarkistajaksi joku muu projektilainen.
Kun tarkistaja on hyväksynyt muutoksen, tehtävä voidaan mergetä masteriin.